package com.bart;

import com.bart.dto.SocketRecieveDTO;
import com.bart.dto.SocketSendDTO;
import com.bart.socket.SocketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BartTestApplicationTests {

	Logger logger = LoggerFactory.getLogger(BartTestApplicationTests.class);

	@Autowired
	SocketService socketService;

	@Test
	public void contextLoads() throws Exception {
		String accountDetail = socketService.getAccountDetail(1L);
		logger.info("accountDetail = {} ", accountDetail);

	}

}
