package com.bart.service;

import com.bart.entity.PostFileEntity;
import com.bart.repository.PostFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by balabaev on 26.10.2017.
 */
@Service
public class PostFileService {

    @Autowired
    PostFileRepository fileRepository;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public PostFileEntity save(PostFileEntity obj){
        return fileRepository.saveAndFlush(obj);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PostFileEntity> findAll(){
        return fileRepository.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PostFileEntity findById(Long id){
        return fileRepository.findById(id);
    }

}
