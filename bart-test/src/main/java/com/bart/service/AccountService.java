package com.bart.service;

import com.bart.entity.AccountEntity;
import com.bart.repository.AccountRepository;
import com.bart.socket.SocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by balabaev on 26.10.2017.
 */
@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    SocketService socketService;

    @PostConstruct
    public void init(){

        AccountEntity obj = new AccountEntity();
        obj.setUsername("admin");
        obj.setPassword("admin");
        save(obj);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public AccountEntity save(AccountEntity obj){
        return accountRepository.saveAndFlush(obj);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public AccountEntity create(AccountEntity obj) throws Exception{
        obj = accountRepository.saveAndFlush(obj);
        obj.setAccountDetail(socketService.getAccountDetail(obj.getId()));
        return accountRepository.save(obj);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteById(Long id){
        accountRepository.delete(id);
    }


    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<AccountEntity> findAll(){
        return accountRepository.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public AccountEntity findOne(Long id){
        return accountRepository.findOne(id);
    }



}
