package com.bart.socket;


import com.bart.dto.SocketRecieveDTO;
import com.bart.dto.SocketSendDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by bart on 28.10.2017.
 */
@Service
public class SocketService {

    Logger logger = LoggerFactory.getLogger(SocketService.class);

    @Value("${socket.server.host}")
    private String host;

    @Value("${socket.server.port}")
    private Integer port;


    public SocketService() {
    }

    public String getAccountDetail(Long id) throws Exception{

        SSLSocketFactory tlsSocketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        Socket clientSocket = tlsSocketFactory.createSocket(host, port);

        String accountDetails = "";


        ObjectMapper mapper = new ObjectMapper();
        String request = mapper.writeValueAsString(new SocketSendDTO(id)) + "\r\n";
        logger.debug("request = {}", request);


        BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        PrintWriter pw = new PrintWriter(clientSocket.getOutputStream(), true);

        pw.println(request);
        String response =  br.readLine();
        logger.debug("response = {}", response);

        SocketRecieveDTO resultDto = mapper.readValue(response, SocketRecieveDTO.class);

        br.close();
        pw.close();
        clientSocket.close();

        return resultDto.getAccountDetails();
    }


}
