package com.bart.controller;

import com.bart.entity.AccountEntity;
import com.bart.entity.PostFileEntity;
import com.bart.service.PostFileService;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by balabaev on 26.10.2017.
 */
@RestController
@RequestMapping("/api")
public class PostFileController {

	@Autowired
	PostFileService fileService;

 	@GetMapping("/files")
	@ApiOperation(value = "Список файлов")
	public List<PostFileEntity>  findAll(){
		return fileService.findAll();
	}

	@PostMapping("/files")
	@ApiOperation(value = "Добавление файла")
	public PostFileEntity save(@RequestParam("file") MultipartFile file/*, Model model*/) throws Exception{
        String fileName = file.getOriginalFilename();
		String contentType = file.getContentType();
		PostFileEntity fileEntity = new PostFileEntity(null, fileName, contentType, file.getBytes());
		return fileService.save(fileEntity);
	}


	@GetMapping("/files/download")
	@ResponseBody
	@ApiOperation(value = "Скачивание файла")
	public void document(@RequestParam("id") Long id, HttpServletResponse response) throws Exception {
		PostFileEntity fileEntity = fileService.findById(id);
		if (fileEntity == null)
			throw new Exception("Нет файла с выбранным id = "+ id);

		response.setContentType(fileEntity.getContentType());
		response.setContentLength(fileEntity.getFileData().length);
		response.setHeader("Content-Disposition","attachment; filename=" + fileEntity.getFileName());

		FileCopyUtils.copy(fileEntity.getFileData(), response.getOutputStream());

	}


}
