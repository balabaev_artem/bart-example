package com.bart.controller;

import com.bart.dto.AccountDTO;
import com.bart.dto.DeleteResult;
import com.bart.entity.AccountEntity;
import com.bart.service.AccountService;
import com.bart.socket.SocketService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;
import java.util.List;

/**
 * Created by balabaev on 26.10.2017.
 */
@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    AccountService accountService;

    @Autowired
    SocketService socketService;

    @PostMapping(value = "/account")
    @ApiOperation(value = "Добавление нового account")
    public AccountEntity insert(@RequestBody AccountDTO dto) throws Exception{

        AccountEntity obj = new AccountEntity(null, null, dto.getUsername(), dto.getPassword());
        return accountService.create(obj);
    }

    @PutMapping(value = "/account")
    @ApiOperation(value = "Изменение account")
    public AccountEntity update(@RequestBody AccountEntity accountObject) throws Exception{

        AccountEntity obj = accountService.findOne(accountObject.getId());

        if (obj == null)
            throw new Exception("Object not found");

        if (!StringUtils.hasLength(accountObject.getAccountDetail()))
            accountObject.setAccountDetail(obj.getAccountDetail());

        return  accountService.save(accountObject);
    }

    @DeleteMapping(value = "/account")
    @ApiOperation(value = "Удаление account")
    public DeleteResult delete(@PathVariable("id") Long id) throws Exception{
        DeleteResult result = new DeleteResult();
        accountService.deleteById(id);
        result.setRowDeleted(1);
        return result;
    }

    @GetMapping(value = "/account")
    @ApiOperation(value = "Список account")
    public List<AccountEntity> findAll(){
        return  accountService.findAll();
    }
}
