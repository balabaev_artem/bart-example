package com.bart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by balabaev on 26.10.2017.
 */
@Controller
@ApiIgnore
public class HomeContoller {


    @GetMapping(value = "/")
    public RedirectView  showSwagger(){
        return new RedirectView("swagger-ui.html");
    }
}
