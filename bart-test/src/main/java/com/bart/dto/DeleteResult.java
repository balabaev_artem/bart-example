package com.bart.dto;

import lombok.Data;

/**
 * Created by balabaev on 26.10.2017.
 */
@Data
public class DeleteResult {
    private Integer rowDeleted;
}
