package com.bart.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by bart on 28.10.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {
    @JsonProperty(required = true)
    private String username;
    @JsonProperty(required = true)
    private String password;
}
