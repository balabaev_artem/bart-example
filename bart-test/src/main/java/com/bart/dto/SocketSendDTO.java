package com.bart.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * Created by bart on 28.10.2017.
 */
@Data
@AllArgsConstructor
@ToString(callSuper=true, includeFieldNames=true)
public class SocketSendDTO {
    private Long id;
}
