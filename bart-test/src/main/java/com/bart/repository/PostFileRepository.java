package com.bart.repository;

import com.bart.entity.PostFileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by bart on 28.10.2017.
 */
@Repository
public interface PostFileRepository extends JpaRepository<PostFileEntity, Long> {
    public PostFileEntity findById(Long id);
}
