package com.bart.repository;

import com.bart.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by balabaev on 26.10.2017.
 */
@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    public AccountEntity findByUsername(String username);

    public AccountEntity findByUsernameAndPassword(String username, String password);
}
